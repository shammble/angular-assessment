import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StarWarsServiceService {

  starWarsApiUrl:string='https://swapi.co/api'

  constructor(private http:HttpClient) { }
  
  //declaring methods

  //inital loading of the data in the file. will bring all users back as default
  getData(){
    return this.http.get(`${this.starWarsApiUrl}/people/1`)
    .pipe(catchError(this.handleError<Object[]>('getInitalData', [])));

  }

  //will change the URL based on the users search criteria
  changeUrl(id,category)
  {

    return this.http.get(`${this.starWarsApiUrl}/${category}/${id}/`)
    .pipe(catchError(this.handleError<Object[]>('getNewData', [])));

  }
//handling error to mostly deal with the URL issues.
  private handleError<T> (operation = 'operation', result?: T) {
    
    console.info('test'+HttpClient.toString)
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      
      // Let the app keep running by returning an empty result.
      return throwError('an error has occured. find the solutionfor error:\n');
    };
  }

}
