import { Component } from '@angular/core';
import { StarWarsServiceService } from './star-wars-service.service';

//components
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  //declaring models
  title = 'Star Wars API Retriever: angular-assessment-brian-clare';
  users
  frmId:number
  whichCategory:string='people'
  choices:Object[]=[{cat:'people'},{cat:'vehicles'},{cat:'starships'},{cat:'planets'},{cat:'species'}]
  model
  
  
  //declare functions
  constructor(private starWarsServiceService:StarWarsServiceService){  }
  handleClick()
  {
    console.info('invoking data for ID:'+this.frmId+' category:'+this.whichCategory)
    //invoke service handling parameters
    this.starWarsServiceService.changeUrl(this.frmId,this.whichCategory).subscribe((result)=>{this.model=result});
  }
  //call method from our service
  invokeService()
  {
    this.starWarsServiceService.getData().subscribe((result)=>{console.log(result)
      this.model=result} );
  }
  //inital prompts when page is loaded
  ngOnInit()
  {
    console.info('initalising data')
    this.invokeService();
  }
  
}
